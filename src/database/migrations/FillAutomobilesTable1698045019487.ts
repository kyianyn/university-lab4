import {MigrationInterface, QueryRunner } from "typeorm";

export class FillAutomobilesTable1698045019487 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO automobiles (name, "manufacturerId", max_speed) VALUES
            ('Toyota Camry', 3, 180),
            ('Honda Civic', 3, 170),
            ('Ford Mustang', 2, 250),
            ('Chevrolet Corvette', 2, 280),
            ('BMW 3 Series', 1, 220),
            ('Audi A4', 1, 200),
            ('Mercedes-Benz E-Class', 1, 240),
            ('Tesla Model S', 2, 200),
            ('Lamborghini Aventador', 1, 350),
            ('Ferrari 488', 1, 330);
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("automobiles");
    }
}