import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateAutomobilesTable1698042711650 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "automobiles",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "name",
                    type: "varchar",
                },
                {
                    name: "manufacturerId",
                    type: "int",
                },
                {
                    name: "max_speed",
                    type: "int",
                },
                {
                    name: "updated_at",
                    type: "timestamp with time zone",
                    default: "now()"
                },
                {
                    name: "created_at",
                    type: "timestamp with time zone",
                    default: "now()"
                }
            ],
            foreignKeys: [
                {
                    columnNames: ["manufacturerId"],
                    referencedTableName: "manufacturers",
                    referencedColumnNames: ["id"],
                    onDelete: "CASCADE",
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("automobiles");
    }
}
