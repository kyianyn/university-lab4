import { MigrationInterface, QueryRunner } from "typeorm";

export class FillManufacturersTable1698043249661 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO manufacturers (name) VALUES
            ('European Auto Concern'),
            ('CarCo. Est.'),
            ('AutoUnion')
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("manufacturers");
    }
}