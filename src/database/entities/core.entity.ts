import { BaseEntity, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export abstract class CoreEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @UpdateDateColumn({
        name: "updated_at",
        type: "timestamp with time zone",
        default: new Date().toISOString()
    })
    updatedAt: Date;

    @CreateDateColumn({
        name: "created_at",
        type: "timestamp with time zone",
        default: new Date().toISOString()
    })
    createdAt: Date;
}