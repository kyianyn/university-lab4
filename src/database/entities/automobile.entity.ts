import {Column, Entity, ManyToOne} from "typeorm";
import { CoreEntity } from "./core.entity";
import {ManufacturerEntity} from "./manufacturer.entity";

@Entity("automobiles")
export class AutomobileEntity extends CoreEntity {
    @Column({
        type: "varchar"
    })
    name: string;

    @ManyToOne(() => ManufacturerEntity, (manufacturer) => manufacturer.automobiles, {
        onDelete: "CASCADE"
    })
    manufacturer: ManufacturerEntity;

    @Column({
        type: "numeric",
        name: "max_speed"
    })
    maxSpeed: number;
}