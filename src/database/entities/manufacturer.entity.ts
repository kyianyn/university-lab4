import { CoreEntity } from "./core.entity";
import { Column, Entity, OneToMany } from "typeorm";
import { AutomobileEntity } from "./automobile.entity";

@Entity("manufacturers")
export class ManufacturerEntity extends CoreEntity {
    @Column({
        type: "varchar"
    })
    name: string;

    @OneToMany(() => AutomobileEntity, (automobile) => automobile.manufacturer, {
        cascade: true
    })
    automobiles: AutomobileEntity[];
}