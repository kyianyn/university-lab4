import { DataSource } from "typeorm";
import {databaseConfiguration} from "./data-config";

export const AppDataSource: DataSource = new DataSource(databaseConfiguration(true));