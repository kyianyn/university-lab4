import "dotenv/config";
import app from "./application/app";

const port = process.env.SERVER_PORT;
const startServer = () => {
    try {
        console.log(`Starting server on ${port} port...`);

        app.listen(port, () => {
            console.log(`Server started on ${port} port.`);
        });
    } catch (e) {
        console.log(e);
    }
}

startServer();