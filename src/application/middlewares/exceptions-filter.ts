import HttpException from "../exceptions/http-exception";
import {NextFunction, Request, Response} from "express";
import {HttpStatusCode} from "../enums/https-statuses";

export const exceptionsFilter = (error: HttpException, req: Request, res: Response, next: NextFunction) => {
    const status: number = error.status || HttpStatusCode.INTERNAL_SERVER_ERROR;
    const message: string = error.message || "Something went wrong";

    res.status(status).json({status, message});
    next();
};

export default exceptionsFilter;