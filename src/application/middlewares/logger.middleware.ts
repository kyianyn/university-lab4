import { NextFunction, Request, Response } from "express";

export const loggerMiddleware = (request: Request, response: Response, next: NextFunction) => {
    const { method, originalUrl } = request;
    let color: string;

    switch (method) {
        case "GET":
            color = "\x1b[35m";
            break;
        case "POST":
            color = "\x1b[32m";
            break;
        case "PATCH":
            color = "\x1b[33m";
            break;
        case "DELETE":
            color = "\x1b[31m";
            break;
        default:
            color = "\x1b[0m";
    }

    console.log(
        `[\x1b[36m${new Date().toISOString()}\x1b[0m] - ${color}${method}\x1b[0m - ${originalUrl}`,
    );

    next();
};