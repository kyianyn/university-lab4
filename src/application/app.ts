import express, { Express } from "express";
import cors from "cors";
import notFoundMiddleware from "./middlewares/not-found.middleware";
import { loggerMiddleware } from "./middlewares/logger.middleware";
import exceptionsFilter from "./middlewares/exceptions-filter";
import { HttpStatusCode } from "./enums/https-statuses";
import {AppDataSource} from "../database/data-source";
import {MoreThanOrEqual, Repository} from "typeorm";
import {ManufacturerEntity} from "../database/entities/manufacturer.entity";
import {AutomobileEntity} from "../database/entities/automobile.entity";

const app: Express = express();

app.use(cors());
app.use(express.json());
app.use(loggerMiddleware);

AppDataSource.initialize();

const manufacturersRepository: Repository<ManufacturerEntity> = AppDataSource.getRepository(ManufacturerEntity);

const automobilesRepository: Repository<AutomobileEntity> = AppDataSource.getRepository(AutomobileEntity);

app.get("/", (req, res) => {

   return res.send(`URL examples
      "/manufacturers"
      "/automobiles"
      "/automobiles/?speed=200"
      "/automobiles/?speed=320"
   `);
});

app.get("/manufacturers", async (req, res) => {
   try {
      const manufacturers: ManufacturerEntity[] = await manufacturersRepository.find();

      return res.json(manufacturers ?? []);
   } catch (e) {
      return res.status(500);
   }
   return res.status(400);
});

app.get("/automobiles", async (req, res) => {
   try {
      const dbQuery: any = {
         where: {},
         relations: ["manufacturer"]
      }

      if (req.query?.speed) {
         dbQuery.where["maxSpeed"] = MoreThanOrEqual(Number(req.query?.speed))
      }

      const automobiles: AutomobileEntity[] = await automobilesRepository.find(dbQuery);

      return res.json(automobiles ?? []);

   } catch (e) {
      return res.status(500);
   }
   return res.status(400);
});

app.use(notFoundMiddleware);
app.use(exceptionsFilter);

export default app;